`default_nettype none

module epp(
	input wire iclk,
	input wire iastb,
	input wire idstb,
	input wire iwr,
	inout wire [7:0] iodata,
	output reg owait,

	output reg owritedone,
	output reg oreaddone,

	output reg [3:0] oaddr,
	input wire [7:0] idata,
	output reg [7:0] odata,

	output reg [3:0] otxcnt,
	output reg [3:0] orxcnt);


//==========  SYNC ASYNC STROBE LINES ================================
	
	wire astb, dstb, wr;

	synchronizer #(.N(1), .INIT(1)) sync_epp_addr(
		.async(iastb), 
		.clk(iclk), 
		.out(astb));

	synchronizer #(.N(1), .INIT(1)) sync_epp_data(
		.async(idstb), 
		.clk(iclk), 
		.out(dstb));

	synchronizer #(.N(1), .INIT(1)) sync_epp_nrw(
		.async(iwr), 
		.clk(iclk), 
		.out(wr));

//===========  STATE MACHINE =========================================

	localparam EPP_STATE_IDLE = 0;
	localparam EPP_STATE_WRITE_DONE = 1;
	localparam EPP_STATE_READ_WAIT = 2;
	localparam EPP_STATE_READ_DONE = 3;

	reg addr_access, addr_access_next;
	reg [2:0] state;
	reg [2:0] state_next;
	reg oe;
	reg [7:0] data;

	initial begin
		state = EPP_STATE_IDLE;
		oe = 0;
		data = 0;
		owait = 0;
		owritedone = 0;
		oreaddone = 0;
		otxcnt = 0;
		orxcnt = 0;
		oaddr = 0;
		addr_access = 0;
	end

	/* 
	 * If `oe == 0` set HiZ state and read from this bus.
	 * Otherwise, set `epp_data` to `epp_data_reg`, thus computer reqested
	 * read operation.
	 */
	assign iodata = oe ? data : 8'hZZ;

	always @(posedge iclk) begin
		case (state)
			EPP_STATE_READ_DONE: begin
				if (astb && dstb) begin
					state <= EPP_STATE_IDLE;
					if (!addr_access)
						oreaddone <= 1;
				end
			end
			EPP_STATE_WRITE_DONE: begin	
				if (astb && dstb) begin
					state <= EPP_STATE_IDLE;
					if (!addr_access)
						owritedone <= 1;
				end
			end

			EPP_STATE_READ_WAIT: begin	 
				state <= EPP_STATE_READ_DONE;
			end

			//=== STATE IDLE
			default: begin
				owritedone <= 0;
				oreaddone <= 0;
				addr_access <= 0;
				if (!astb) begin  
					addr_access <= 1;
					if (!wr) begin
						// PC => FPGA - address
						// Read directly from bus
						state <= EPP_STATE_WRITE_DONE;
						oaddr <= iodata;
					end 
					else begin
						// PC <= FPGA - address
						state <= EPP_STATE_READ_WAIT;
						data <= oaddr;
					end
				end
				else if (!dstb) begin  
					if (!wr) begin
						// PC => FPGA - address
						// Read directly from bus
						state <= EPP_STATE_WRITE_DONE;
						odata <= iodata;
						orxcnt <= orxcnt + 1;
					end 
					else begin
						state <= EPP_STATE_READ_WAIT;
						data <= idata;
						otxcnt <= otxcnt + 1;
					end
				end
			end
		endcase
	end

	//==== CONTROL OUTPUTS
	
	always @(state) begin
		case (state)
			EPP_STATE_WRITE_DONE: begin
				oe <= 0;
				owait <= 1;
			end

			EPP_STATE_READ_WAIT: begin
				oe <= 1;	
				owait <= 0;
			end

			EPP_STATE_READ_DONE: begin
				oe <= 1;	
				owait <= 1;
			end

			default: begin // IDLE
				oe <= 0;	
				owait <= 0;
			end
		endcase
	end


endmodule

