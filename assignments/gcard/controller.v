`default_nettype none

module controller (
	input wire iclk,
	// EPP
	input wire ieppwdone,
	input wire iepprdone,
	input wire [3:0] ieppaddr,
	input wire [7:0] ieppdata,
	output wire [7:0] oeppdata,
	// VGA
	output reg ovgawe,
	output reg ovgaen,
	output reg [12:0] ovgaaddr,
	input wire [7:0] ivgadata,
	output reg [7:0] ovgadata,
	// UI
	input wire [3:0] ibtn, // synced, on press
	input wire [7:0] isw,
	output wire [6:0] oseg,
	output wire [3:0] oan
);

	initial begin
		ovgaen = 1;
		ovgawe = 0;
		ovgaaddr = 0;
		ovgadata = 0;
	end

//====== REGISTERS
	/*
		00: pierwsza współrzędna X, dolny bajt
		01: pierwsza współrzędna X, górny bajt
		02: pierwsza współrzędna Y, dolny bajt
		03: pierwsza współrzędna Y, górny bajt
		04: druga współrzędna X, dolny bajt
		05: druga współrzędna X, górny bajt
		06: druga współrzędna Y, dolny bajt
		07: druga współrzędna Y, górny bajt
		08: szerokość operacji, dolny bajt
		09: szerokość operacji, górny bajt
		0a: wysokość operacji, dolny bajt
		0b: wysokość operacji, górny bajt

		0c: rejestr uruchomienia operacji blit
		0d: rejestr uruchomienia operacji fill
		0e: rejestr dostępu do bufora ramki
		0f: rejestr statusu
	*/

	reg [7:0] regs [15:0];
       	reg [3:0] ridx = 0;

	integer j;
	initial begin
		ridx = 0;
		for (j = 0; j < 16; j = j + 1)
			regs[j] = 0;
	end

//======== UI & DEBUG

	reg [15:0] regs_lookup;

	always @(posedge iclk) begin
		regs_lookup <= {ridx, regs[ridx]};
		if (ibtn[0])
			ridx <= ridx + 1;
		if (ibtn[1])
			ridx <= ridx - 1;
	end

	display display_inst(
		.clk(iclk), 
		.number(regs_lookup), 
		.common(oan), 
		.err(0), 
		.segments(oseg)
		);

//====== EPP

	// READ REGISTERS - ALWAYS AVAILABLE
	assign oeppdata = regs[ieppaddr];

	localparam STATE_IDLE = 0;
	localparam STATE_FILL_0 = 1;
	localparam STATE_FILL_1 = 2;
	localparam STATE_FILL_2 = 3;
	localparam STATE_FILL_LOAD_0 = 4;
	localparam STATE_FILL_LOAD_1 = 5;
	localparam STATE_FILL_LOAD_2 = 6;
	localparam STATE_FILL_LOAD_3 = 7;
	localparam STATE_BLIT_0 = 8;
	localparam STATE_BLIT_LOAD_0 = 9;
	localparam STATE_BLIT_LOAD_1 = 10;
	localparam STATE_BLIT_LOAD_2 = 11;
	localparam STATE_BLIT_LOAD_3 = 12;
	localparam STATE_BLIT_LOAD_4 = 13;
	localparam STATE_BLIT_COPY_0 = 14;
	localparam STATE_BLIT_NEXT_LINE = 15;

	reg [3:0] state = STATE_IDLE;

	// Fill, Blit dest
	reg [7:0] upline, downline, lineidx; 	
	reg [8:0] lpix, rpix;
	reg [5:0] lbyte, rbyte, byteidx;
	reg [7:0] lfill, rfill;
	reg [7:0] lbuff, rbuff;

	// Blit source
	reg [7:0] upline_, downline_, lineidx_;
	reg [8:0] lpix_, rpix_;
	reg [5:0] lbyte_, rbyte_, byteidx_;

	reg [7:0] linebuffer [39:0];

	reg [3:0] shift;
	reg shiftright;
	reg [23:0] r;

	reg v0, v1, v2;
	reg [7:0] d2, d3;
	reg [5:0] i0, i1, i2;
	initial begin
		v0 = 0;
		v1 = 0;
		v2 = 0;
		d2 = 0;
		d3 = 0;
		i0 = 0;
		i1 = 0;
		i2 = 0;
	end

	// COMMUNICATION HANDLING 
	always @(posedge iclk) begin

		case (state)
		STATE_IDLE: begin
			// Always update address 
			ovgaaddr <= ((regs[1] << 5) | (regs[0] >> 3)) + 13'd40 * ((regs[3] << 8) | (regs[2]));
			regs[4'he] <= ivgadata;
			ovgawe <= 0;

			if (ieppwdone) begin
				if (ieppaddr < 4'hc) 
					// Write params - ALWAYS
					regs[ieppaddr] <= ieppdata;
				else if (ieppaddr == 4'hc) begin
					// Start BLIT 
					state <= STATE_BLIT_0;
					regs[4'hf] <= 1;
				end
				else if (ieppaddr == 4'hd) begin
					// Start FILL 
					regs[ieppaddr] <= ieppdata;	// Store color
					regs[4'hf] <= 1;
					state <= STATE_FILL_0;
				end
				else if (ieppaddr == 4'he) begin
					// Write to framebuffer
					ovgadata <= ieppdata;
					ovgawe <= 1;
					// ovgaaddr has correct address already
					// Move to next byte, and it will be updated
					// in two cycles from now, when data are stored already
					if ({regs[1], regs[0]} >= 312) begin
						{regs[1], regs[0]} <= 0;
						if ({regs[3], regs[2]} == 199)
							{regs[3], regs[2]} <= 0;
						else
							{regs[3], regs[2]} <= {regs[3], regs[2]} + 1;
					end
					else
						{regs[1], regs[0]} <= {regs[1], regs[0]} + 8;
				end
				// 0x0f - status - READ ONLY
			end 

			if (iepprdone) begin
				if (ieppaddr == 4'he) begin
					// Read from framebuffer
					// ovgaaddr has correct address already
					// regs[0x0e] has appropriate value already
					// Just move to next byte
					if ({regs[1], regs[0]} == 312) begin
						{regs[1], regs[0]} <= 0;
						if ({regs[3], regs[2]} == 199)
							{regs[3], regs[2]} <= 0;
						else
							{regs[3], regs[2]} <= {regs[3], regs[2]} + 1;
					end
					else
						{regs[1], regs[0]} <= {regs[1], regs[0]} + 8;
				end
			end
		end // END OF STATE_IDLE 
		
		//===========================================================
		//== FILL OPERATION

		STATE_FILL_0: begin
			lpix = {regs[1], regs[0]};
			rpix = lpix + {regs[9], regs[8]}; 
			lbyte = lpix >> 3;
			rbyte = rpix >> 3;
			lfill = 8'hff << (lpix - (lbyte << 3));
			rfill = ~(8'hff << (rpix - (rbyte << 3)));
			upline = {regs[3], regs[2]};
			downline = upline + {regs[11], regs[10]};
			byteidx <= lbyte;
			lineidx <= upline;

			if (downline == upline || lpix == rpix) begin // H or W is zero
				state <= STATE_IDLE;
				regs[4'hf] <= 0;
			end else begin
				state <= STATE_FILL_LOAD_0;
			end
		end

		STATE_FILL_LOAD_0: begin
			state <= STATE_FILL_LOAD_1;
			ovgaaddr <= lbyte + lineidx * 13'd40; // Get left overlapping buffer
		end

		STATE_FILL_LOAD_1: begin
			state <= STATE_FILL_LOAD_2;
			ovgaaddr <= rbyte + lineidx * 13'd40; // Get right overlapping buffer
		end

		STATE_FILL_LOAD_2: begin
			state <= STATE_FILL_LOAD_3;
			lbuff <= ivgadata;
		end

		STATE_FILL_LOAD_3: begin
			state <= STATE_FILL_1;
			rbuff <= ivgadata;
		end

		STATE_FILL_1: begin // FILL SINGLE LINE
			ovgaaddr <= byteidx + lineidx * 13'd40;
			ovgawe <= 1;
			if (byteidx == lbyte && lbyte == rbyte) begin
				if (regs[4'hd])
					ovgadata <= lbuff | (lfill & rfill);
				else
					ovgadata <= lbuff & ~(lfill & rfill);
			end
			else if(byteidx == lbyte) begin
				if (regs[4'hd])
					ovgadata <= lbuff | lfill;
				else
					ovgadata <= lbuff & ~lfill;
			end
			else if(byteidx == rbyte) begin
				if (regs[4'hd])
					ovgadata <= rbuff | rfill;
				else
					ovgadata <= rbuff & ~rfill;
			end
			else begin
				if (regs[4'hd])
					ovgadata <= 8'hff;
				else
					ovgadata <= 0;
			end
			byteidx <= byteidx + 1;

			if (byteidx == rbyte)
				state <= STATE_FILL_2;
		end

		STATE_FILL_2: begin
			ovgawe <= 0;
			byteidx <= lbyte;
			lineidx <= lineidx + 1;
			if (lineidx >= downline - 1) begin
				state <= STATE_IDLE;
				regs[4'hf] <= 0;
			end
			else
				state <= STATE_FILL_LOAD_0; // Go back and load overlapping bytes again
		end

		//===========================================================
		//== BLIT OPERATION
		
		STATE_BLIT_0: begin
			// Destination
			lpix = {regs[1], regs[0]};
			rpix = lpix + {regs[9], regs[8]}; 
			lbyte = lpix >> 3;
			rbyte = rpix >> 3;
			upline = {regs[3], regs[2]};
			downline = upline + {regs[11], regs[10]};
			// Source
			lpix_ = {regs[5], regs[4]};
			rpix_ = lpix_ + {regs[9], regs[8]}; 
			lbyte_ = lpix_ >> 3;
			rbyte_ = rpix_ >> 3;
			upline_ = {regs[7], regs[6]};
			downline_ = upline_ + {regs[11], regs[10]};
						
			shiftright = (lpix & 7) < (lpix_ & 7) ? 0 : 1;
			shift = (lpix & 7) < (lpix_ & 7) ? (lpix_ & 7) - (lpix & 7) : (lpix & 7) - (lpix_ & 7);

			byteidx <= lbyte;
			byteidx_ <= lbyte_;
			lineidx <= upline < upline_ ? upline : downline - 1;
			lineidx_ <= upline < upline_ ? upline_ : downline_ - 1;

			lfill = 8'hff << (lpix - (lbyte << 3));
			rfill = ~(8'hff << (rpix - (rbyte << 3)));
			if (downline == upline || lpix == rpix) begin // H or W is zero
				state <= STATE_IDLE;
				regs[4'hf] <= 0;
			end else begin
				state <= STATE_BLIT_LOAD_0;
			end
		end

		STATE_BLIT_LOAD_0: begin
			ovgaaddr <= lbyte + lineidx * 13'd40; // Get left overlapping buffer
			state <= STATE_BLIT_LOAD_1;
		end

		STATE_BLIT_LOAD_1: begin
			ovgaaddr <= rbyte + lineidx * 13'd40; // Get right overlapping buffer
			state <= STATE_BLIT_LOAD_2;
		end

		STATE_BLIT_LOAD_2: begin 
			lbuff <= ivgadata;
			state <= STATE_BLIT_LOAD_3;
		end

		STATE_BLIT_LOAD_3: begin 
			rbuff <= ivgadata;
			state <= STATE_BLIT_LOAD_4;
		end

		STATE_BLIT_LOAD_4: begin
			// 1 <= 0
			ovgaaddr <= byteidx_ + lineidx_ * 13'd40; 
			byteidx_ <= byteidx_ + 1;
			v0 <= byteidx_ <= rbyte_ ? 1 : 0;
			i0 <= byteidx_;
			// 2 <= 1
			v1 <= v0;
			i1 <= i0;
			// 3 <= 2, first data ready in VGA output
			i2 <= i1;
			v2 <= v1;
			d2 <= ivgadata;
			// 4 <= 3
			d3 <= d2;

			if (v2) begin // if v3 == 1, then {ivgadata, d2, d3} forms 3 byte vga line
				if (i2 == lbyte_) begin
					if (shiftright)
						r = {ivgadata, d2, 8'h00} << shift;
					else
						r = {ivgadata, d2, 8'h00} >> shift;

				end
				else if (i2 == rbyte_) begin
					if (shiftright)
						r = {8'h00, d2, d3} << shift;
					else
						r = {8'h00, d2, d3} >> shift;
				end
				else begin
					if (shiftright)
						r = {ivgadata, d2, d3} << shift;
					else
						r = {ivgadata, d2, d3} >> shift;
				end
				linebuffer[byteidx] <= r[15:8];
				byteidx <= byteidx + 1;
				if (byteidx == rbyte) begin
					state <= STATE_BLIT_COPY_0;
					byteidx <= lbyte;
				end
			end

		end // END OF STATE_BLIT_LOAD_4

		STATE_BLIT_COPY_0: begin
			ovgawe <= 1;
			ovgaaddr <= byteidx + 13'd40 * lineidx;
			byteidx <= byteidx + 1;

			// Apply masks
			if (byteidx == lbyte && lbyte == rbyte) begin
				ovgadata <= (~(lfill & rfill) & lbuff) | ((lfill & rfill) & linebuffer[byteidx]);
			end
			else if (byteidx == lbyte) begin
				ovgadata <= (~lfill & lbuff) | (lfill & linebuffer[byteidx]);
			end
			else if (byteidx == rbyte) begin
				ovgadata <= (~rfill & rbuff) | (rfill & linebuffer[byteidx]);
			end 
			else
				ovgadata <= linebuffer[byteidx];

			if (byteidx == rbyte)
				state <= STATE_BLIT_NEXT_LINE;
		end

		STATE_BLIT_NEXT_LINE: begin
			ovgawe <= 0;
			v0 <= 0;
			v1 <= 0;
			v2 <= 0;
			lineidx <= upline < upline_ ? lineidx + 1 : lineidx - 1;
			lineidx_ <= upline < upline_ ? lineidx_ + 1 : lineidx_ - 1;
			byteidx <= lbyte;
			byteidx_ <= lbyte_;
			if ((upline < upline_ && lineidx == downline - 1) || (upline >= upline_ && lineidx == upline)) begin
				state <= STATE_IDLE;
				regs[4'hf] <= 0;
			end 
			else
				state <= STATE_BLIT_LOAD_0;

		end

		endcase
	end

endmodule

