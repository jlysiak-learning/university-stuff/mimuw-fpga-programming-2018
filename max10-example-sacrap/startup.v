`default_nettype none

module startup(input [15:14] D, input CLK_10M, output [3:0] LED);
	
	wire clk_50M;
	wire e;
	wire [3:0] out;
	assign e = 1;
	
	clk_50 pll_50M(.inclk0(CLK_10M), .c0(clk_50M));
	
	counter #(4, 20) counter_0 (.clk(clk_50M), .enable(e), .reset(~D[14]), .out(out));
	assign LED = ~out;
endmodule
