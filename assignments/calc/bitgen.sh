#!/bin/bash

mkdir -p out
cd out

xst -ifn ../calc.xst && \
ngdbuild calc -uc ../calc.ucf && \
map calc && \
par -w calc.ncd calc_par.ncd &&\
bitgen -w calc_par.ncd -g StartupClk:JTAGClk

