`default_nettype none

module counter #(parameter OUT_SIZE = 4, DIV_N = 20) (input clk, input enable, input reset, output [OUT_SIZE-1:0] out);

	reg [OUT_SIZE-1:0] main_counter;
	reg [DIV_N-1:0] div_counter;

	assign out = main_counter;

	initial main_counter = 0;
	initial div_counter = 0;
	
	always @ (posedge clk, posedge reset)
	begin: COUTNER_N_DIVIDER
		if (reset) begin
			main_counter <= 0;
			div_counter <= 0;
		end 
		else if (enable) begin
			div_counter <= div_counter + 1;
			if (div_counter == (1 << DIV_N) - 1)
				main_counter <= main_counter + 1;
		end
	end

endmodule
