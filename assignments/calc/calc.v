`default_nettype none
module calc(input [7:0] sw, input [3:0] btn, output [7:0] Led);
	reg [7:0] ab;
	wire [3:0] a;
	wire [3:0] b;
	wire [3:0] div_res;
        wire [3:0] div_rem;
        assign a = sw[7:4];
        assign b = sw[3:0];

	divider #(4) div(.a(a), .b(b), .res(div_res), .rem(div_rem));
	
	always @ (btn, a, b) begin
		case (btn)
			4'b1000: begin
				ab[7:4] <= div_res;
				ab[3:0] <= div_rem;
			end // 1000
			
			4'b0100: begin
				ab <= a * b;
			end // 0100
			
			4'b0010: begin
				if (a > b) begin
					ab[7:4] <= a;
					ab[3:0] <= b;
				end 
				else begin
					ab[7:4] <= b;
					ab[3:0] <= a;
				end
			end // 0010
			
			4'b0001: begin
				ab[7:4] <= a + b;
				ab[3:0] <= a - b;
			end // 0001
			
			default: begin
				ab <= 0;
			end	
		endcase // case (btn)
	end // always @ (btn)
	
	assign led = ab;
endmodule
