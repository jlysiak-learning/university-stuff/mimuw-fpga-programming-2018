`default_nettype none

module clock_div #(parameter N=5) (input clkin,  output clkout);
        localparam CNT_W = 1 << N;

	reg [CNT_W-1:0] cnt;
	reg x;

	assign clkout = x;

        initial begin
                cnt = 0;
                x = 0;
        end
	
	always @ (posedge clkin)
	begin
                if (cnt == (2 ** CNT_W) - 1) begin
                        x <= x + 1;
                        cnt <= 0;
                end else
                        cnt <= cnt + 1;
        end
endmodule
	
