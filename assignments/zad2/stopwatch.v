`default_nettype none

module stopwatch
	#(parameter 
		W=10,
		TOP=1023,
		BOTTOM=0
	)(
		input clk,
		input ce,
		input rst,
		input stop,
		input up,
		input down,
		output reg[W-1:0] counter,
		output reg[2:0] signals 
	);
	
	localparam STATE_STOP = 0;
	localparam STATE_UP = 1;
	localparam STATE_DOWN = 2;
	localparam STATE_END = 3;

	reg [1:0] state;
	reg [1:0] next_state;

	initial begin
		state = STATE_STOP;
		next_state = STATE_STOP;
		counter = 0;
		signals = 0;
	end

	always @(posedge clk) begin
		if (rst == 1) begin
			counter <= 0;
			state <= STATE_STOP;
			signals <= 3'b000;
		end else 
		begin
			if (stop == 1) begin
				state <= STATE_STOP;
				signals <= 3'b000;
			end 
			else if (down == 1) begin
				state <= STATE_DOWN;
				signals <= 3'b001;
			end
			else if (up == 1) begin
				state <= STATE_UP;
				signals <= 3'b010;
			end 
			if (ce) begin
				case (state) 
					STATE_UP: begin
						if (counter < TOP) 
							counter <= counter + 1;
						else begin
							state <= STATE_STOP;
							signals <= 3'b100;
						end
					end // case: STATE_UP

					STATE_DOWN: begin
						if (counter > BOTTOM) 
							counter <= counter - 1;
						else begin
							state <= STATE_STOP;
							signals <= 3'b100;

						end
					end // STATE_DOWN
				endcase
			end
		end
	end
endmodule

