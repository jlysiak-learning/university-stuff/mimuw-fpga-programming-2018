#!/bin/bash

QCOW=/mnt/slowy/pul2018_cow.qcow2
THIS=`pwd`

echo $THIS

qemu-system-x86_64 \
        -enable-kvm \
	-smp 6\
        -drive file=${QCOW},if=virtio \
        -usb \
        -net nic,model=virtio \
        -net user,hostfwd=tcp::10022-:22 \
        -m 1G \
        -fsdev local,id=pul2018-share,path=${THIS},security_model=none \
        -device virtio-9p-pci,fsdev=pul2018-share,mount_tag=pul2018-share \
        -display none\
        -chardev stdio,id=cons,signal=on\
        -device virtio-serial-pci\
        -device virtconsole,chardev=cons

        #-device usb-host,vendorid=0x1443,productid=0x0007 \
