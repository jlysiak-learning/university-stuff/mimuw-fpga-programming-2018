module divider(input clk, output out);
	parameter N = 21;

	reg [N:0] cnt;
	reg x;
	
	assign out = x;
	
	always @ (posedge clk)
	begin: DIV
		cnt <= cnt + 1;
		if (cnt == 0)
			x <= x ^ 1;
			
	end
	
endmodule
	
