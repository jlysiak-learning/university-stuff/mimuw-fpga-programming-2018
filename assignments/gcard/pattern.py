#!/usr/bin/env python3


import numpy as np

W = 320
H = 200

arr = np.zeros((H, W))
arr[:,0] = 1
arr[:,W-1] = 1
arr[H-1,:] = 1
arr[0,:] = 1

arr = arr.flatten().astype(np.uint8)

v = 0
f = 0

xlat = bytes(
    sum(1 << bit for bit in range(8) if x & 1 << (7 - bit))
    for x in range(0x100)
)

for i, x in enumerate(arr):
    v = v << 1 | x
    if i % 8 == 7:
        v = xlat[v]
        print(f'{v:02x}')
        v = 0

for _ in range(8000, 2 ** 13):
    print("00")
