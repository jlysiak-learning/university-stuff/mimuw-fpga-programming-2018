# Karta graficzna

Jacek Łysiak <jl345639@students.mimuw.edu.pl>

### Wymagania

- środowisko pul2018 image
- https://github.com/jlysiak/adepttool.git -b jl/basys2project

### Synteza

- `make`

### Programowanie

- `make prog`

### Uwagi

- `fill.sh posX posY W H C` - wypełnia prostokąt na kolor `C`, wszystkie liczby są HEXami - `XX` albo `0xXX`  
- `blit.sh` - rysuje prostokąt, kopiuje i wkleja  
- Implementacja wykorzystuje oscylator 50MHz (bo nie miałem innego). 
  Obraz jest niestalibny i czasami nie współpracuje ze starszymi monitorami.
- `pattern.py` - prosty skrypt do inicjalizacji framebuffer-a.

