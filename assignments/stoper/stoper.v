`default_nettype none

module stoper(input clk_50M, input [5:0] sw, input [3:0] btn, output [2:0] led, output [3:0] an, output [6:0] seg);

        /* Synchronized user signals */
        wire [3:0] sync_btn;
	wire [5:0] sync_sw;

        /* Selected clock source. */
        wire clk_in;
	/* Prescaled clock - stopwatch clock */
        wire clk_en;

	/* Stopwatch state inticators */
        wire [2:0] stopwatch_signals;
        assign led[2:0] = stopwatch_signals;

	/* Stopwatch counter */
	wire [15:0] stopwatch_counter;
	/* Stopwatch counter in BCD format */
	wire [15:0] stopwatch_bcd;

	/* Select external clock source. */
       // BUFGMUX ext_clock_mux(.O(clk_in), .I0(clk_50M), .I1(clk_ext), .S(sync_sw[5]));
       assign clk_in = clk_50M;

        /* Synchronizer blocks */
	genvar i;
	generate
		for (i = 0; i < 4; i = i + 1) begin: btn_sync
			synchronizer sync_btn(.async(btn[i]), .clk(clk_in), .out(sync_btn[i]));
		end
		for (i = 0; i < 6; i = i + 1) begin: sw_sync
			synchronizer sync_sw(.async(sw[i]), .clk(clk_in), .out(sync_sw[i]));
		end
	endgenerate

        /* User controlled prescaler. */
	prescaler #(.W(5)) main_clk_prescaler(.clk_in(clk_in), .ce(clk_en), .div(sync_sw[4:0]));

	/* Stopwatch module */
	stopwatch #(.W(16), .TOP(9999), .BOTTOM(0)) main_stopwatch(.clk(clk_in), .ce(clk_en), .rst(sync_btn[3]), .stop(sync_btn[2]), .up(sync_btn[1]), .down(sync_btn[0]), .counter(stopwatch_counter), .signals(stopwatch_signals));

	/* Convert counter to BCD */
	bintobcd #(.W(16), .D(4)) counter_bcd(.val(stopwatch_counter), .out(stopwatch_bcd));

	/* Use 50MHz clock to keep display timings constant. */
        display_mux #(4) display_mux_0 (.clk(clk_in), .digits(stopwatch_bcd), .common(an), .segments(seg));

endmodule
