`default_nettype none

module synchronizer 
	#(
		parameter INIT = 0,
		parameter N = 2
	) (
		input async, 
		input clk, 
		output out
	);

        reg tmp1 = INIT;
	reg tmp2 = INIT;
        reg _out = INIT;

        always @(posedge clk) begin
                tmp1 <= async;
		if (N == 1) 
			_out <= tmp1;
		else begin
			tmp2 <= tmp1;
			_out <= tmp2;
		end
        end
        assign out = _out;
endmodule
