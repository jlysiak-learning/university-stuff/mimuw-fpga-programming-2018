`default_nettype none

module display #(parameter N=4, W=16) (input [W-1:0] val, input [7:0] display_segments, input [N-1:0] displays_comm);

        wire [N-1:0] comm;// Common pins in single displays
        wire [7:0] digits [N-1:0]; // bcd encoded digits

        genvar i;

        generate 
                for(i = 0; i < N; i = i + 1) begin : generate_displays
                        bintobcd #(i) bintobcd_inst(.val(val), .out(digits[i]));
                        seg_disp seg_disp_inst(.val(digits[i]), .segments(display_segments));
                end
        endgenerate

endmodule

