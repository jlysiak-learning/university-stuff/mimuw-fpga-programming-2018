`default_nettype none

module display_mux #(parameter N=4) (input clk, input [4*N-1:0] digits, output reg [N-1:0] common, output [6:0] segments);
        localparam COUNTER_EXP = 17;
        localparam SETUP_TIME = 2 ** 15;
        localparam SHOW_TIME = 2 ** 16;
        localparam HOLD_TIME = 2 ** 15;


        wire [3:0] digits_u [N-1:0];
        genvar un_idx;
        generate
                for (un_idx = 0; un_idx < N; un_idx = un_idx + 1) begin : digits_unpack
                        assign digits_u[un_idx] = digits[4*(un_idx+1)-1:4*un_idx];
                end
        endgenerate

        reg [3:0] digit_value;
        reg [3:0] digit_active; // Max 8 segments
        reg [1:0] state;
        reg [COUNTER_EXP-1:0] counter;

        initial begin
                counter = 0;
                common = 2**N -1;
                state = 0;
                digit_active = 0;
        end
       
        // BCD to 7segment display
        seg7 seg7_0(.v(digit_value), .o(segments));

        // Multiplexing 7segment display
        always @(posedge clk) begin
                if (counter == (SETUP_TIME + SHOW_TIME + HOLD_TIME - 1)) begin
                        counter <= 0;
                        state <= 0;
                end else begin
                        if (counter == SETUP_TIME - 1)
                                state <= 1;
                        else if (counter == (SETUP_TIME + SHOW_TIME - 1)) begin
                                state <= 2;
                                if (digit_active == N - 1)
                                        digit_active <= 0;
                                else
                                        digit_active <= digit_active + 1; // ONLY IN SYNCHRONOUS!! NEVER IN COMBINATORIAL BLOCK LIKE BEFORE!!!
                        end
                        counter <= counter + 1;
                end
        end

        always @(state) begin
                case (state)
                        0: begin
                                common = 2 ** N - 1;
                                digit_value = digits_u[digit_active];
                        end
                        1: begin
                                common = (2 ** N - 1) ^ (2 ** digit_active);
                        end
                        2: begin
                                common = 2 ** N - 1;

                                // NEVER DO THAT !!!!!!!!!!!!!!!!!!
                                //if (digit_active == N - 1)
                                //        digit_active <= 0;
                                //else
                                //        digit_active <= digit_active + 1; // ONLY IN SYNCHRONOUS!! NEVER IN COMBINATORIAL BLOCK LIKE BEFORE!!!
                        end
                endcase
        end
endmodule
