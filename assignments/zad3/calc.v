`default_nettype none

module stoper(input clk_50M, input [7:0] sw, input [3:0] btn, output [7:0] led, output [3:0] an, output [6:0] seg);

	localparam W = 32;

        /* Synchronized user signals */
        wire [3:0] sync_btn;
	wire [7:0] sync_sw;

        /* Synchronizer blocks */
	genvar i;
	generate
		for (i = 0; i < 4; i = i + 1) begin: btn_sync
			synchronizer sync_btn(.async(btn[i]), .clk(clk_50M), .out(sync_btn[i]));
		end
		for (i = 0; i < 8; i = i + 1) begin: sw_sync
			synchronizer sync_sw(.async(sw[i]), .clk(clk_50M), .out(sync_sw[i]));
		end
	endgenerate


	/*================ STACK */
	wire [6:0] stack_size;
	wire [W-1:0] stack_top;
	wire stack_empty;
	stack stack();

	/*================ STACK MACHINE */
	wire error;

	/*================ LEDS */
	assign led[6:0] = stack_size;
	assign led[7] = error;

	/*================ DISPLAY */

	wire [W/2-1:0] half_hi;
	wire [W/2-1:0] half_lo;
	reg [W/2-1:0] _digits;
	wire [W/2-1:0] digits;

	assign half_hi = stack_top[W-1:W/2];
	assign half_lo = stack_top[W/2-1:0];
	assign digits = _digits;
	
	/* Display - Half-word selection */
	always @ (sync_btn[0]) begin
		if (sync_btn[0])
			_digits = half_hi;
		else
			_digits = half_lo;
	end
        display_mux #(4) display_mux_0 (.clk(clk_50M), .digits(digits), .err(stack_empty), .common(an), .segments(seg));

endmodule
