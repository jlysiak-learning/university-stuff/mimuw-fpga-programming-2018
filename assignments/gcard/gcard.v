`default_nettype none

module gcard (
	input wire mclk,
	input wire [7:0] sw,
	input wire [3:0] btn,
	output wire [7:0] Led,
	output wire HSYNC,
	output wire VSYNC,
	output wire [2:0] OutRed,
	output wire [2:0] OutGreen,
	output wire [1:0] OutBlue,
	inout wire [7:0] EppDB,
	input wire EppAstb,
	input wire EppDstb,
	input wire EppWR,
	output wire EppWait,
	output wire [6:0] seg,
	output wire [3:0] an);

//====== CLK
	
	// Main clk
	wire clk;

	// mclk = 50MHz
	DCM_SP #(
		.CLKFX_DIVIDE(4),
		.CLKFX_MULTIPLY(2),
		.CLKIN_PERIOD(20.00),
		.CLK_FEEDBACK("NONE"),
		.STARTUP_WAIT("TRUE")
	) dcm_vclk (
		.CLKFX(clk),
		.CLKIN(mclk)
	);

//====== UI

	wire [7:0] ssw;
	wire [3:0] sbtn;
	reg [3:0] btn_old = 0;
	reg [3:0] btn_press = 0;

	genvar i;
	generate
		for (i = 0; i < 4; i = i + 1) begin: btn_sync
			synchronizer sync_btn(.async(btn[i]), .clk(clk), .out(sbtn[i]));
		end
		for (i = 0; i < 8; i = i + 1) begin: sw_sync
			synchronizer sync_btn(.async(sw[i]), .clk(clk), .out(ssw[i]));
		end

		for (i = 0; i < 4; i = i + 1) begin: on_btn_press
			always @(posedge clk) begin
				btn_old[i] <= sbtn[i];
				btn_press[i] <= 0;
				if (sbtn[i] && !btn_old[i]) begin
					btn_press[i] <= 1;
				end
			end
		end
	endgenerate

//====== EPP -- instructions

	wire [3:0] epp_tx_cnt, epp_rx_cnt;
	wire [3:0] epp_addr;
	wire epp_write_done, epp_read_done;
	wire [7:0] epp_idata;
	wire [7:0] epp_odata;
	
	epp epp_inst(
		.iclk(clk),
		.iastb(EppAstb),
		.idstb(EppDstb),
		.iwr(EppWR),
		.iodata(EppDB),
		.owait(EppWait),

		.owritedone(epp_write_done),
		.oreaddone(epp_read_done),

		.oaddr(epp_addr),
		.idata(epp_idata),
		.odata(epp_odata),

		.otxcnt(epp_tx_cnt),
		.orxcnt(epp_rx_cnt));

	assign Led = {epp_tx_cnt, epp_rx_cnt};

//====== VGA
	
	wire vga_we;
	wire vga_en;
	wire [12:0] vga_addr; // byte addr
	wire [7:0] vga_idata;
	wire [7:0] vga_odata;

	vga vga_inst (
		.HS(HSYNC),
		.VS(VSYNC),
		.R(OutRed),
		.G(OutGreen),
		.B(OutBlue),
		.iclk(clk),

		.iwe(vga_we),
		.ien(vga_en),
		.iaddr(vga_addr),
		.idata(vga_idata),
		.odata(vga_odata)
		);

//====== CONTROLLER 
	
	controller controller_inst(
		.iclk(clk),
		// EPP
		.ieppwdone(epp_write_done), 
		.iepprdone(epp_read_done),
		.ieppaddr(epp_addr),
		.ieppdata(epp_odata),
		.oeppdata(epp_idata),
		// VGA
		.ovgawe(vga_we),		
		.ovgaen(vga_en),		
		.ovgaaddr(vga_addr),
		.ivgadata(vga_odata),
		.ovgadata(vga_idata),
		// UI
		.ibtn(btn_press),
		.isw(ssw),
		.oseg(seg),
		.oan(an)
		);

endmodule

