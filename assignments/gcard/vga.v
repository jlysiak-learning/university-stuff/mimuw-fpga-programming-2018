`default_nettype none

module vga(
	input wire iclk,
	output HS,
	output VS,
	output reg [2:0] R,
	output reg [2:0] G,
	output reg [1:0] B,
	input wire iwe,
	input wire ien,
	input wire [12:0] iaddr, // Byte address
	input wire [7:0] idata,
	output wire [7:0] odata
	);


	/*
	VGA Signal 640 x 400 @ 70 Hz timing
	Pixel freq.	25.175 MHz

	============= HORIZONTAL TIMMING ========

	Scanline part	Pixels	Time [µs]
	Visible area	640	25.422045680238
	Front porch	16	0.63555114200596
	Sync pulse	96	3.8133068520357
	Back porch	48	1.9066534260179
	Whole line	800	31.777557100298

	*/
	parameter H_TOTAL = 100;
	parameter H_VISIBLE = 80;
	parameter H_SS = 82;
	parameter H_SE = 94;

	/*
	parameter H_TOTAL = 168;
	parameter H_VISIBLE = 128;
	parameter H_SS = 131;
	parameter H_SE = 148;
	*/

	/*
	============= VERTICAL TIMMING ========

	Frame part	Lines	Time [ms]
	Visible area	400	12.711022840119
	Front porch	12	0.38133068520357
	Sync pulse	2	0.063555114200596
	Back porch	35	1.1122144985104
	Whole frame	449	14.268123138034
	*/

	parameter V_TOTAL = 449;
	parameter V_VISIBLE = 400;
	parameter V_SS = 412;
	parameter V_SE = 414;

	/*
	parameter V_TOTAL = 806;
	parameter V_VISIBLE = 768;
	parameter V_SS = 771;
	parameter V_SE = 777;
	*/

	//========= VGA 
	
	reg [2:0] hpix;	// 0 : 7 bits
	reg [8:0] hpos;	// 0 : H_TOTAL - 1
	reg [9:0] vpos;	// 0 : V_TOTAL - 1
	initial begin
		hpos = 0;
		vpos = 0;
		hpix = 0;
	end

	assign HS = ~(hpos >= H_SS && hpos < H_SE);
	assign VS = (vpos >= V_SS && vpos < V_SE);
	wire de;
	assign de = hpos < H_VISIBLE && vpos < V_VISIBLE;

	always @(posedge iclk) begin
		if (hpix == 7) begin
			hpix <= 0;
			if (hpos == H_TOTAL - 1) begin
				hpos <= 0;
				if (vpos == V_TOTAL - 1)
					vpos <= 0;
				else
					vpos <= vpos + 1;
			end else begin
				hpos <= hpos + 1;
			end
		end else begin
			hpix <= hpix + 1;
		end
	end

	//======== FRAMEBUFFER
	
	/*
	*  Screen resolution: 640 x 400
	*  Frame buffer: 320 x 200 = 64000 bits -> 4 x 16000 bits
	*  320 x 200 = 64000 bits = 8000 B
	*/

	reg [12:0] vgaaddr;
	wire [7:0] vgaline;
	reg [2:0] pix, pix_2;
	reg de_1, de_2;

	ram ram_inst(
		.iclk(iclk),
		.ienA(1),
		.iweA(0),
		.iaddrA(vgaaddr),
		.idataA(0),
		.odataA(vgaline),
		.ienB(ien),
		.iweB(iwe),
		.iaddrB(iaddr),
		.idataB(idata),
		.odataB(odata));

	//== VGA access to FB 
	always @(posedge iclk) begin
		// 1 <= 0
		pix <= (hpos[0] << 2) | (hpix >> 1);
		vgaaddr <= (hpos >> 1) + (vpos >> 1) * 13'd40;
		de_1 <= de;

		// 2 <= 1
		pix_2 <= pix;
		de_2 <= de_1;
		//  3 <= 2 (vga line available)
		if (de_2) begin
			R <= vgaline[pix_2] ? 7 : 0;
			G <= vgaline[pix_2] ? 7 : 0;
			B <= vgaline[pix_2] ? 7 : 0;
		end
		else begin
			R <= 0;
			G <= 0;
			B <= 0;
		end
	end
endmodule

