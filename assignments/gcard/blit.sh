#!/bin/bash


./fill.sh 5 5 45 45 1

# Horizontal copy
basys2_epp -p 0 55 # Xd
basys2_epp -p 1 0

basys2_epp -p 2 5 # Yd
basys2_epp -p 3 0

basys2_epp -p 4 5 # Xs
basys2_epp -p 5 0

basys2_epp -p 6 5 # Ys
basys2_epp -p 7 0

basys2_epp -p 8 45
basys2_epp -p 9 0

basys2_epp -p a 45
basys2_epp -p b 0

basys2_epp -p c 1

sleep 3

# Vertical copy
basys2_epp -p 0 5 # Xd
basys2_epp -p 1 0

basys2_epp -p 2 55 # Yd
basys2_epp -p 3 0

basys2_epp -p c 1

sleep 3

basys2_epp -p 4 25 # Xs
basys2_epp -p 5 0

basys2_epp -p 6 25 # Ys
basys2_epp -p 7 0

basys2_epp -p 8 45
basys2_epp -p 9 0

basys2_epp -p a 45
basys2_epp -p b 0

basys2_epp -p 0 60 # Xd
basys2_epp -p 1 0

basys2_epp -p 2 15 # Yd
basys2_epp -p 3 0

basys2_epp -p c 1
