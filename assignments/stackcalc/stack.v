`default_nettype none

module stack(
	input clk,
	input push,
	input pop,
	input [31:0] datain,		// Input data
	output [31:0] dataout,		// Value on the stack top
	output reg [9:0] size,		// Stack size
	output empty,			// 1 - stack empty, top value invalid
	output reg err,			// Last operation failed
	output reg fin);		// Operation finished


	reg ram_en, ram_we; 		// Enable, Write Enable
	reg [8:0] ram_addr;

	initial begin
		ram_en = 0;
		ram_we = 0;
		ram_addr = 0;
		size = 0;
		err = 0;
		fin = 0;
	end

	assign empty = size == 0 ? 1 : 0;

	ram ram0(.clk(clk), .en(ram_en),  .we(ram_we), .addr(ram_addr), 
			.in(datain), .out(dataout));
	
	reg [2:0] state = 0;
	always @(posedge clk) begin
		case (state) 
			0: begin // PUSH, POP = 0
				if (push) begin
					if (size == 512) begin
						err <= 1;
						fin <= 1;
					end
					else begin
						err <= 0;
						ram_en <= 1;
						ram_we <= 1;
						if (size > 0)
							ram_addr <= ram_addr + 1;
						state <= 1;
						size <= size + 1;
						fin <= 0;
					end
				end
				else if (pop) begin
					if (size == 0) begin
						err <= 1;
						fin <= 1;
					end
					else begin
						err <= 0;
						ram_en <= 1;
						ram_we <= 0;
						if (ram_addr > 0)
							ram_addr <= ram_addr - 1;
						state <= 2;
						size <= size - 1;
						fin <= 0;
					end
				end
			end
			1: begin
				ram_en <= 1;
				ram_we <= 0;
				state <= 2;
			end
			2: begin
				state <= 0;
				ram_en <= 0;
				ram_we <= 0;
				fin <= 1;
			end
		endcase
	end
endmodule

