`default_nettype none

module ram(
        input wire iclk,
	input wire ienA,
	input wire iweA,
	input wire [12:0] iaddrA,
	input wire [7:0] idataA,
	output reg [8:0] odataA,
	input wire ienB,
	input wire iweB,
	input wire [12:0] iaddrB,
	input wire [7:0] idataB,
	output reg [8:0] odataB);

	localparam SIZE = 1 << 13;

	reg [7:0] mem[SIZE-1:0];
	initial $readmemh("data/framebuffer.bin", mem);


	always @(posedge iclk) begin
		if (ienA) begin
			if (iweA)
				mem[iaddrA] <= idataA;
			odataA <= mem[iaddrA];
		end
	end

	always @(posedge iclk) begin
		if (ienB) begin
			if (iweB)
				mem[iaddrB] <= idataB;
			odataB <= mem[iaddrB];
		end
	end

endmodule



