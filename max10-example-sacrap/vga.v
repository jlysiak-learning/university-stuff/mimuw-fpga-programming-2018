// VGA 800x600 timmings 
// Cycles of 50MHz clock

`define FRONT_PORCH			56
`define SYNC_PULSE			120
`define BACK_PORCH			64

`define LINE_WIDTH 			800
`define LINES 					600

`define LINES_FRONT_PORCH	37
`define LINES_SYNC_PORCH	6
`define LINES_BACK_PORCH	23


module VGA800x600(input clk_50, output [2:0] vga_rgb, output vga_hsync, output vga_vsync);

	integer cnt_x;
	integer cnt_y;
	
	initial cnt_x = - (FRONT_PORCH + SYNC_PULSE + BACK_PULSE);
	initial cnt_y = - (FRONT_PORCH + SYNC_PULSE + BACK_PULSE);
	
	reg hsync;
	reg vsync;
	reg [2:0] rgb;
	
	always @ (posedge clk_50)
	begin : FRAME
		cnt_x <= cnt_x + 1;
		if cnt_x < -(SYNC_PULSE + BACK_PORCH)
			hsync <= 0;
		else if cnt_x < -BACK_PORCH
			hsync <= 1;
		else if cnt_x < 0;
			hsync <= 0;
		else if cnt_x < LINE_WIDTH;
		begin
			if 300 < cnt_x && cnt_x < 500
				rgb <= 3'b111;
			else
				rgb <= 3'b000;
		end
		else
		begin
			cnt_x <= - (FRONT_PORCH + SYNC_PULSE + BACK_PULSE);
			cnt_y <= cnt_y + 1;
		end
		
			
	end

endmodule
	