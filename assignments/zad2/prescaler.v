`default_nettype none

module prescaler 
	#(parameter W=5)
	(
		input clk_in, 
		output reg ce, 
		input [W-1:0] div
	);

	reg [2 ** W - 1:0] _cnt;

	initial begin
		ce = 0;
		_cnt = 0;
	end

	always @(posedge clk_in) begin
		if (_cnt >= (1 << div - 1)) begin
			_cnt <= 0;
			ce <= 1;
		end else begin
			_cnt <= _cnt + 1;
			ce <= 0;
		end
	end

endmodule

