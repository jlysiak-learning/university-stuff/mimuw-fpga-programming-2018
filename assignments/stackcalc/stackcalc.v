`default_nettype none

module stackcalc (
	input wire mclk, 
	input wire [7:0] sw, 
	input [3:0] btn, 
	output [3:0] an,
	output [6:0] seg,
	output [7:0] Led);

	wire [7:0] sync_sw;
	wire [3:0] sync_btn;

	genvar i;
	generate
		for (i = 0; i < 4; i = i + 1) begin: btn_sync
			synchronizer sync_btn(.async(btn[i]), .clk(mclk), .out(sync_btn[i]));
		end
		for (i = 0; i < 8; i = i + 1) begin: sw_sync
			synchronizer sync_btn(.async(sw[i]), .clk(mclk), .out(sync_sw[i]));
		end
	endgenerate

//========== ERRORS 

	wire stack_err;
	reg this_err = 0;

	assign Led[7] = stack_err | this_err;

//========== DISPLAY

	wire stack_empty;

	reg [15:0] display_number = 0;
	display display0(.clk(mclk), .number(display_number), .common(an), 
				.err(stack_empty), .segments(seg));

//========== STACK

	wire stack_fin;
	wire stack_push;
	wire stack_pop;
	wire [9:0] stack_size;
	wire [31:0] stack_top;
	reg [31:0] stack_datain;

	initial stack_datain = 0;

	assign Led[6:0] = stack_size;

	stack stack0(
			.clk(mclk), 
			.push(stack_push), 
			.pop(stack_pop), 
			.datain(stack_datain), 
			.dataout(stack_top), 
			.size(stack_size), 
			.empty(stack_empty), 
			.err(stack_err),
			.fin(stack_fin));

//========== STACK BASIC OPS

	wire stack_op_fin;
	reg stack_op_start;
	reg [3:0] stack_op;

	initial begin
		stack_op_start = 0;
		stack_op = 0;
	end

	stack_ops stack_ops0(
			.clk(mclk), 
			.start(stack_op_start), 
			.op(stack_op), 
			.stack_fin(stack_fin),
			.stack_push(stack_push),
			.stack_pop(stack_pop),
			.op_fin(stack_op_fin));

//========== RESET
	
	reg GSR_PORT = 0;
	STARTUP_SPARTAN3E STARTUP_SPARTAN3E_inst ( .GSR(GSR_PORT) );

//========== DIVIDER 

	reg divider_start = 0;
	reg [31:0] arg0;
	reg [31:0] arg1;
	wire [31:0] divider_res;
	wire [31:0] divider_rem;
	wire divider_fin;

	divider #(.WIDTH(32)) divider0(
			.clk(mclk), 
			.start(divider_start), 
			.a(arg0),
			.b(arg1),
			.res(divider_res),
			.rem(divider_rem),
			.fin(divider_fin));

//========== STATE MACHINE

	localparam STATE_IDLE = 0;
	localparam STATE_START_OP = 1;		
	localparam STATE_WAIT = 2;		
	localparam STATE_POP_ARG0_0 = 3;
	localparam STATE_POP_ARG0_1 = 4;
	localparam STATE_POP_ARG0_2 = 5;
	localparam STATE_POP_ARG1_0 = 6;	
	localparam STATE_POP_ARG1_1 = 7;

	localparam STATE_ARGS_OP = 8;	
	localparam STATE_WAIT_DIV = 9;
	localparam STATE_WAIT_MOD = 10;
	localparam STATE_SWAP_0 = 11;
	localparam STATE_SWAP_1 = 12;
	localparam STATE_SWAP_2 = 13;


	localparam OP_PUSH = 0;
	localparam OP_CHANGE = 1;
	localparam OP_POP = 2;
	
	reg [2:0] args_op;
	reg [2:0] args_left;

	reg [3:0] last_btn;
	reg [4:0] state;
	reg [2:0] op;
	initial begin
		last_btn = 0;
		state = 0;
		op = 0;
		args_left = 0;
	end

	always @(posedge mclk) begin

	//======== DISPLAY
		
		if (sync_btn[0]) 
			display_number <= stack_top[31:16];
		else
			display_number <= stack_top[15:0];

	//======== RESET
		
		if (sync_btn[0] && last_btn[3]) begin
			GSR_PORT <= 1;
		end

	//======== STATE MACHINE	
		
		case (state) 
		STATE_IDLE: begin 

			//==== PUSH 8 bits STACK
			if (sync_btn[1] && !last_btn[1]) begin
				stack_datain <= sync_sw;
				stack_op <= OP_PUSH;
				state <= STATE_START_OP;
				this_err <= 0;
			end 
			//==== APPEND 8 bits STACK
			else if (sync_btn[2] && !last_btn[2]) begin
				if (stack_size) begin
					stack_datain <= stack_top << 8 | sync_sw;
					state <= STATE_START_OP;
					stack_op <= OP_CHANGE;
					this_err <= 0;
				end else
					this_err <= 1;
			end 
			//==== DO OTHER OPERATIONS
			else if (sync_btn[3] && !last_btn[3]) begin
				args_op <= sync_sw[2:0]; 
				this_err <= 0;

				if (sync_sw[2:0] == 3'b101) begin //== POP
					state <= STATE_START_OP;
					stack_op <= OP_POP;
				end
				else if (sync_sw[2:0] == 3'b110) begin //== DUP 
					if (stack_size == 0) 
						this_err <= 1;
					else begin
						stack_datain <= stack_top;
						state <= STATE_START_OP;
						stack_op <= OP_PUSH;
					end
				end
				else begin
					if (stack_size < 2)
						this_err <= 1;
					else begin
						arg1 <= stack_top;
						state <= STATE_POP_ARG0_0;
						stack_op <= OP_POP;
					end
				end
			end 
		end

		STATE_POP_ARG0_0: begin
			stack_op_start <= 1;
			state <= STATE_POP_ARG0_1;	
		end

		STATE_POP_ARG0_1: begin
			stack_op_start <= 0;
			state <= STATE_POP_ARG0_2;	
		end

		STATE_POP_ARG0_2: begin
			if (stack_op_fin) begin
				state <= STATE_POP_ARG1_0;
				arg0 <= stack_top;
				stack_op_start <= 1;
			end
		end

		STATE_POP_ARG1_0: begin
			stack_op_start <= 0;
			state <= STATE_POP_ARG1_1;
		end

		STATE_POP_ARG1_1: begin
			if (stack_op_fin)
				state <= STATE_ARGS_OP;
		end

		STATE_START_OP: begin
			stack_op_start <= 1;
			state <= STATE_WAIT;	
		end

		STATE_WAIT: begin
			stack_op_start <= 0;
			if (stack_op_fin) begin // Stack operation DONE
				state <= STATE_IDLE;	
			end
		end

		STATE_ARGS_OP: begin
			case (args_op)
				3'b000: begin //== ADD
					stack_datain <= arg0 + arg1;
					stack_op <= OP_PUSH;
					state <= STATE_START_OP;
				end

				3'b001: begin //== SUB
					stack_datain <= arg0 - arg1;
					stack_op <= OP_PUSH;
					state <= STATE_START_OP;
				end

				3'b010: begin //== MUL
					stack_datain <= arg0 * arg1;
					stack_op <= OP_PUSH;
					state <= STATE_START_OP;
				end

				3'b011: begin //== DIV
					if (arg1 == 0) begin
						this_err <= 1;
						stack_datain <= arg0;
						arg0 <= arg1;
						stack_op <= OP_PUSH;
						state <= STATE_SWAP_0;
					end else begin
						state <= STATE_WAIT_DIV;
						divider_start <= 1;
					end
				end

				3'b100: begin //== MOD
					if (arg1 == 0) begin
						this_err <= 1;
						stack_datain <= arg0;
						arg0 <= arg1;
						stack_op <= OP_PUSH;
						state <= STATE_SWAP_0;
					end
					else begin
						state <= STATE_WAIT_MOD;
						divider_start <= 1;
					end
				end

				3'b101: begin //== POP
					// Can't happen
				end

				3'b110: begin //== DUP
					// Can't happen
				end

				3'b111: begin //== SWAP
					stack_datain <= arg1;
					stack_op <= OP_PUSH;
					state <= STATE_SWAP_0;
				end
			endcase
		end

		STATE_SWAP_0: begin
			stack_op_start <= 1;
			state <= STATE_SWAP_1;
		end

		STATE_SWAP_1: begin
			stack_op_start <= 0;
			state <= STATE_SWAP_2;
		end

		STATE_SWAP_2: begin
			if (stack_op_fin) begin
				state <= STATE_START_OP;
				stack_datain <= arg0;
			end
		end

		STATE_WAIT_DIV: begin
			divider_start <= 0;
			if (divider_fin) begin 
				stack_datain <= divider_res;
				stack_op <= OP_PUSH;
				state <= STATE_START_OP;
			end
		end

		STATE_WAIT_MOD: begin
			divider_start <= 0;
			if (divider_fin) begin 
				stack_datain <= divider_rem;
				stack_op <= OP_PUSH;
				state <= STATE_START_OP;
			end
		end

		endcase
		
		//==== BUTTONS LAST STATE
		// Needed to triggerring only on / input 
		last_btn <= sync_btn;
	end
	
endmodule

