#!/bin/bash

mkdir -p out
cd out
xst -ifn ../stoper.xst && \
ngdbuild stoper -uc ../stoper.ucf && \
map stoper && \
par -w stoper.ncd stoper_par.ncd &&\
bitgen -w stoper_par.ncd -g StartupClk:JTAGClk

