`default_nettype none

module display 
	#(parameter D = 4)
	(
		input clk, 
		input [15:0] number,
		input err,
		output [3:0] common,
		output [6:0] segments
	);

	//wire [4*D-1:0] bcd;

	//bintobcd #(.W(16), .D(4)) counter_bcd(.val(number), .out(bcd));

	display_mux #(4) display_mux0 (.clk(clk), .digits(number), .err(err), .common(common), .segments(segments));

endmodule
