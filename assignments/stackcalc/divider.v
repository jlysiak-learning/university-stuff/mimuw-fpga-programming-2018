`default_nettype none

/*
* Better divider...
*/

module divider 
        #(
                parameter WIDTH = 32
        )(
		input clk,
		input start,
                input [WIDTH-1:0] a, 
                input [WIDTH-1:0] b, 
                output reg [WIDTH-1:0] res, 
                output reg [WIDTH-1:0] rem,
		output reg fin
        );

	reg [WIDTH:0] cnt;

	initial begin 
		fin = 0;
		cnt = 0;
	end

	always @(posedge clk) begin
		if (cnt == 0) begin
			if (start) begin
				rem <= a;
				res <= 0;
				cnt <= WIDTH;
				fin <= 0;
			end
		end 
		else begin
                        if (((rem  >> (cnt-1))) >= b) begin
                                rem <= rem - (b << (cnt-1));
                                res[cnt-1] <= 1;
                        end 
                        else begin
                                res[cnt-1] <= 0;
                        end
			cnt <= cnt - 1;
			if (cnt == 1)
				fin <= 1;
		end
	end

endmodule
