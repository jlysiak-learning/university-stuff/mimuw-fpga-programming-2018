`default_nettype none

module bintobcd #(parameter W=16, D=4) (input [W-1:0] val, output [4*D-1:0] out);

	wire [W-1:0] d [D-1:0];
	assign out = {d[3][3:0], d[2][3:0], d[1][3:0], d[0][3:0]};

	reg [W-1:0] b = 10;
	wire [W-1:0] v1;
	wire [W-1:0] v2;
	wire [W-1:0] v3;
	wire [W-1:0] v4;

	divider #(16) div_0(.a(val), .b(b), .res(v1), .rem(d[0]));
	divider #(16) div_1(.a(v1), .b(b), .res(v2), .rem(d[1]));
	divider #(16) div_2(.a(v2), .b(b), .res(v3), .rem(d[2]));
	divider #(16) div_3(.a(v3), .b(b), .res(v4), .rem(d[3]));


endmodule
