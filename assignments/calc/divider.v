`default_nettype none

module divider 
        #(
                parameter WIDTH = 4
        )(
                input [WIDTH-1:0] a, 
                input [WIDTH-1:0] b, 
                output reg [WIDTH-1:0] res, 
                output reg [WIDTH-1:0] rem
        );

        reg [WIDTH-1:0] _a;
        reg [WIDTH-1:0] _res;

        integer i;
        always @(a,b) begin
                _a = a;
                for (i = 0; i < WIDTH; i = i + 1) 
                begin : gen_result
                        if ((_a  >> (WIDTH-i-1)) >= b) begin
                                _a = _a - (b << (WIDTH-i-1));
                                _res[WIDTH-i-1] = 1;
                        end 
                        else begin
                                _res[WIDTH-i-1] = 0;
                        end
                end
		res = _res;
		rem = _a;
        end
endmodule
