`default_nettype none

module stack_ops (
	input clk, 
	input start,
	input [3:0] op,
	input stack_fin,
	output reg stack_push,
	output reg stack_pop,
	output reg op_fin);

	initial begin
		op_fin = 0;
		stack_push = 0;
		stack_pop = 0;
	end

	localparam OP_PUSH = 0;
	localparam OP_CHANGE = 1;	// Xchange TOP value with input
	localparam OP_POP = 2;

	localparam STATE_IDLE 		= 0;
	localparam STATE_PUSH_1 	= 1;
	localparam STATE_PUSH_2 	= 2;
	localparam STATE_PUSH_3 	= 3;
	localparam STATE_CHANGE_1 	= 4;
	localparam STATE_CHANGE_2 	= 5;
	localparam STATE_POP_1 		= 6;
	localparam STATE_POP_2 		= 7;

	reg [4:0] state;
	initial begin
		state = 0;
	end

	always @(posedge clk) begin
		case (state) 
		STATE_IDLE: begin 
			if (start) begin
				if (op == OP_PUSH) begin
					stack_push <= 1;
					state <= STATE_PUSH_1;
				end
				else if (op == OP_CHANGE) begin
					stack_pop <= 1;
					state <= STATE_CHANGE_1;
				end
				else if (op == OP_POP) begin
					stack_pop <= 1;
					state <= STATE_POP_1;
				end
				op_fin <= 0;
			end
		end

		STATE_POP_1: begin
			stack_pop <= 0;
			state <= STATE_POP_2;
		end
		
		STATE_POP_2: begin
			state <= STATE_IDLE;
			op_fin <= 1;
		end

		STATE_PUSH_1: begin
			stack_push <= 0;
			state <= STATE_PUSH_2;
		end

		STATE_PUSH_2: begin
			state <= STATE_PUSH_3;
		end
		
		STATE_PUSH_3: begin
			state <= STATE_IDLE;
			op_fin <= 1;
		end

		STATE_CHANGE_1: begin
			stack_pop <= 0;
			state <= STATE_CHANGE_2;
		end

		STATE_CHANGE_2: begin
			stack_push <= 1;
			state <= STATE_PUSH_1;
		end
		endcase
	end
endmodule

