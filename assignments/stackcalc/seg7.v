`default_nettype none

/*
* 7-segment display
*/

module seg7(input [3:0] v, input err, output reg [6:0] o);
        always @(v, err) begin
		if (err) 
			o <= 7'h3f;
		else begin
			case (v)
				0: o <= 7'h40;
				1: o <= 7'h79;
				2: o <= 7'h24;
				3: o <= 7'h30;
				4: o <= 7'h19;
				5: o <= 7'h12;
				6: o <= 7'h02;
				7: o <= 7'h78;
				8: o <= 7'h00;
				9: o <= 7'h10;
				10: o <= 7'h08; // A
				11: o <= 7'h03; // b
				12: o <= 7'h27; // c
				13: o <= 7'h21; // d
				14: o <= 7'h06; // E
				15: o <= 7'h0e; // F
			endcase
		end
        end
endmodule

