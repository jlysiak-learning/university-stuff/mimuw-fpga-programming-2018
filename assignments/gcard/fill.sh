#!/bin/bash


# prog X Y W H C
basys2_epp -p 0 $1
basys2_epp -p 1 0
basys2_epp -p 8 $3
basys2_epp -p 9 0

basys2_epp -p 2 $2
basys2_epp -p 3 0
basys2_epp -p a $4
basys2_epp -p b 0

basys2_epp -p d $5

