`default_nettype none

/*
* Stack module
*/
module stack (
	input clk,
	input push,
	input pop,
	input [W-1:0] data,
	input rst,
	output [31:0] top,
	output reg [9:0] size,
	output empty);


// RAMB16_S18_S36: Spartan-3/3E 1k/512 x 16/32 + 2/4 Parity bits Dual-Port RAM
// Xilinx HDL Libraries Guide, version 11.2
	RAMB16_S18_S36  RAMB16_S18_S36_inst (
			.DOB(top), // Port B 32-bit Data Output
			.ADDRB(ADDRB), // Port B 9-bit Address Input
			.CLKB(clk), // Port B Clock
			.DIB(DIB), // Port B 32-bit Data Input
			.DIPB(DIPB), // Port-B 4-bit parity Input
			.ENB(ENB), // Port B RAM Enable Input
			.SSRB(SSRB), // Port B Synchronous Set/Reset Input
			.WEB(WEB) // Port B Write Enable Input
		);

	initial begin
		size = 0;
	end

	assign empty = size == 0 ? 1'b1 : 1'b0;

	always @ (posedge clk) begin


	end



endmodule;


