`default_nettype none

module synchronizer(input async, input clk, output out);
        reg tmp1, tmp2;
        reg _out;
        always @(posedge clk) begin
                tmp1 <= async;
                tmp2 <= tmp1;
                _out <= tmp2;
        end
        assign out = _out;
endmodule
