`default_nettype none

module ram(
	input clk,
	input en,
	input we,
	input [8:0] addr,
	input [31:0] in,
	output [31:0] out);

	reg [31:0] mem[511:0]; 	// RAM
	reg [31:0] data;

	assign out = data;

	integer i;
	initial begin
		for (i = 0; i < 512; i = i + 1)
			mem[i] = i;
		data = 0;
	end

	always @(posedge clk) begin
		if (en && we) begin
			mem[addr] <= in;
		end
	end

	always @(posedge clk) begin
		if (en && !we) begin
			data <= mem[addr];
		end
	end

       /*
       	wire [9:0] _addra;
       	wire [3:0] _dipb;
	wire _wea, _ena, _ssra, _ssrb;
	assign _dipb = 0;
	assign _addra = 0;
	assign _wea = 0;
	assign _ena = 0;
	assign _ssra = 0;
	assign _ssrb = 0;
	RAMB16_S18_S36 RAMB16_S18_S36_inst (
		.DOB(out), // Port B 32-bit Data Output
		.ADDRB(addr), // Port B 9-bit Address Input
		.ADDRA(_addra), // Port B 9-bit Address Input
		.CLKB(clk), // Port B Clock
		.CLKA(_ena), // Port B Clock
		.DIB(in), // Port B 32-bit Data Input
		.ENB(cs), // Port B RAM Enable Input
		.WEB(we),
		.DIPB(_dipb),
		.ENA(_ena),
		.WEA(_wea),
		.SSRA(_ssra),
		.SSRB(_ssrb)); // Port A Write Enable Input
	*/
endmodule

